<!DOCTYPE html>
<html lang="de">

<head>
    <title>URL Shortener</title>
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css" />
    <script>
        async function performAction(action) {
            const response = await fetch(action + generateParams());
            document.getElementById('result').innerHTML = await response.text();
            await refreshList();
        }

        async function refreshList() {
            document.getElementById('list').innerHTML = 'Loading...';
            const response = await fetch('/list');
            document.getElementById('list').innerHTML = await response.text();
        }

        function generateParams() {
            return '?path=' + encodeURIComponent(document.getElementById('path').value)
                + '&location=' + encodeURIComponent(document.getElementById('location').value);
        }

        window.onload = refreshList;
    </script>
</head>

<body>
    <h1>
        <em>
            <?php echo $_SERVER['HTTP_HOST']; ?>
        </em>
        <br />
        URL Shortener
    </h1>

    <?php
    if (isset($_GET['redirect_error'])) {
        echo "<p><em>The called shortcut does not exist.</em></p>";
    }
    ?>

    <em>
        <p id="result"></p>
    </em>
    <form>
        <label for="path">
            <?php echo $_SERVER['HTTP_HOST']; ?>/
        </label>
        <input type="text" id="path" name="path" value="" />
        <br><br>
        <label for="location">Location:</label>
        <input type="text" id="location" name="location" value="" />
        <br><br>
        <button type="button" onclick="performAction('/add')">Add</button>
        <button type="button" onclick="performAction('/remove')">Remove</button>
    </form>
    <h2><em>Current shortcuts</em></h2>
    <p id="list"></p>
</body>

</html>