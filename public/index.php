<?php

$configPath = getcwd() . '/persistence/locations.json';

$path = explode('?', $_SERVER['REQUEST_URI'])[0];

# Root hostname can either be set as environment variable ROOT_HOSTNAME or is calculated
$rootHostname = getenv('ROOT_HOSTNAME');
if ($rootHostname === false) {
    $dotPosition = strpos($_SERVER['HTTP_HOST'], '.');
    $rootHostname = substr($_SERVER['HTTP_HOST'], $dotPosition + 1);
}
$domain = $rootHostname;
$subdomain = substr_replace(str_replace($rootHostname, '', $_SERVER['HTTP_HOST']), '', -1);

if ($subdomain !== '') {
    if ($path === '/') {
        $path = '/' . $subdomain;
    } else {
        header('Location: //' . $domain . $path, true, 303);
        exit();
    }
}

# Start Session
session_set_cookie_params(2592000);
session_start();

# Create config directory if it does not exist
if (!file_exists(dirname($configPath))) {
    mkdir(dirname($configPath), 0777, true);
}

require 'SsoClient.php';

switch ($path) {
    case '/':
        login();
        require_once 'gui.php';
        return exit();

    case '/favicon.ico':
        login();
        header('Content-type: image/x-icon');
        readfile('favicon.ico');
        return exit();

    case '/styles.css':
        login();
        header('Content-type: text/css');
        readfile('styles.css');
        return exit();

    case '/robots.txt':
        login();
        header('Content-type: text/plain');
        readfile('robots.txt');
        return exit();

    case '/logout':
        return SsoClient::logout();

    case '/add':
        login();
        if (!(isset($_GET['path']) && isset($_GET['location']) && strpos($_GET['path'], '/') === false)) {
            exit('Wrong call');
        }
        addShortcut($configPath, '/' . urldecode($_GET['path']), urldecode($_GET['location']));
        if (isset(readShortcuts($configPath)['/' . urldecode($_GET['path'])])) {
            return exit('Successfully registered /' . $_GET['path'] . '::' . $_GET['location']);
        }
        return exit('Registration of ' . urldecode($_GET['path']) . '::' . urldecode($_GET['location']) . ' failed');

    case '/remove':
        return exit("Function 'remove' is disabled.");

    case '/list':
        login();
        $listString = '';
        foreach (readShortcuts($configPath) as $key => $value) {
            if (!empty($key) && !empty($value) && (substr($value, 0, 7) === 'http://' || substr($value, 0, 8) === 'https://')) {
                $listString .= "<a href=\"$key\">$key</a>" . ' => ' . "<a href=\"$value\">$value</a><br><br>";
            }
        }
        echo $listString;
        return;

    default:
        return header('Location: ' . getLocation($domain, $path, readShortcuts($configPath)), true, 303);
}


function login()
{
    if (!SsoClient::isValid()) {
        SsoClient::login();
    }
}

function addShortcut($filePath, $path, $location)
{
    $shortcuts = readShortcuts($filePath);
    $shortcuts[$path] = $location;
    writeShortcuts($filePath, $shortcuts);
}

function getLocation($domain, $path, $shortcuts)
{
    foreach ($shortcuts as $key => $value) {
        if ($path === $key) {
            return $value;
        }
    }
    return '//' . $domain . '/?redirect_error';
}

function readShortcuts($filePath)
{
    if (file_exists($filePath)) {
        return json_decode(file_get_contents($filePath), true);
    } else {
        return array();
    }
}

function writeShortcuts($filePath, $shortcuts)
{
    file_put_contents($filePath, json_encode($shortcuts, JSON_PRETTY_PRINT));
    chmod($filePath, 0777);
}
